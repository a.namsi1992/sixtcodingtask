On this code I implemented a "minimalistic" version of clean architecture with MVVM pattern.

For the sake of example I've only added the necessary layers to keep the code as simple as I can.

I could have added a domaine layer and specified a seperate model for each layer (data, domaine and presentation) and added a mapper between them and even extract each one on a seperate module.

The app contains a map that will hosts the markers, an expandable bottom sheet that contains the list of cars through it we can go to marker on map or to the car details.
It supports dark and light themes and offline mode

## Tech Stack

- Kotlin
- Android X
- Android Architecture Components (ViewModel, LiveData, Room)
- RxJava / RxKotlin / RxAndroid
- Retrofit
- Koin (Dependency injection framework for Kotlin)
- Picasso for Image Loading
- Junit & Mockito for Unit Tests

## Download APK from [HERE](https://drive.google.com/file/d/1sH9W_UGsfNmnoOzRKDRqJyAmYC2ibPDk/view?usp=sharing)
