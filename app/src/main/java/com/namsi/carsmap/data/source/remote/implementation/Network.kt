package com.namsi.carsmap.data.source.remote.implementation

import com.namsi.carsmap.data.source.remote.abstraction.SixtApi
import com.namsi.carsmap.utils.BASE_URL
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


val retrofit: Retrofit =
    createNetworkClient(BASE_URL)
val sixtApi = retrofit.create(SixtApi::class.java)


fun createNetworkClient(baseUrl: String): Retrofit {

    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}