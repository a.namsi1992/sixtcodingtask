package com.namsi.carsmap.data.source.local.abstraction.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.namsi.carsmap.data.model.Car
import io.reactivex.Single

@Dao
interface CarDao {
    @Query("SELECT * from car")
    fun getAllCars(): Single<List<Car>>

    @Query("SELECT * from car WHERE id = :carId")
    fun getCarById(carId: String): Single<Car>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCars(cars: List<Car>)

}