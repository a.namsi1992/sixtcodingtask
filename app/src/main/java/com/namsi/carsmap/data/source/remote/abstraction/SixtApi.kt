package com.namsi.carsmap.data.source.remote.abstraction

import com.namsi.carsmap.data.model.Car
import io.reactivex.Single
import retrofit2.http.GET

interface SixtApi {

    @GET("cars")
    fun getAllCars(): Single<List<Car>>

}