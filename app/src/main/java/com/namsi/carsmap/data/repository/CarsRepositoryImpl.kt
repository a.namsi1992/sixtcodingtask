package com.namsi.carsmap.data.repository

import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.data.source.local.abstraction.CarLocalDataSource
import com.namsi.carsmap.data.source.remote.abstraction.CarRemoteDataSource
import io.reactivex.Single

class CarsRepositoryImpl(
    private val carsLocalDataSource: CarLocalDataSource,
    private val carsRemoteDataSource: CarRemoteDataSource
) : CarsRepository {
    override fun getCars(): Single<List<Car>> =
        carsRemoteDataSource.getCars()
            .doOnSuccess { carsLocalDataSource.addCars(it) }
            .onErrorResumeNext { carsLocalDataSource.getCars() }

    override fun getCarById(carId: String): Single<Car> = carsLocalDataSource.getCarById(carId)

}