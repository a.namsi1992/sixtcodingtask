package com.namsi.carsmap.data.source.local.abstraction.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.data.source.local.abstraction.room.dao.CarDao

@Database(entities = [Car::class], version = 1, exportSchema = false)
abstract class SixtDataBase : RoomDatabase() {

    abstract fun carDao() : CarDao

}