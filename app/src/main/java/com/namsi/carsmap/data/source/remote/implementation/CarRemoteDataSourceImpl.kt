package com.namsi.carsmap.data.source.remote.implementation

import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.data.source.remote.abstraction.SixtApi
import com.namsi.carsmap.data.source.remote.abstraction.CarRemoteDataSource
import io.reactivex.Single

class CarRemoteDataSourceImpl(
    private val api: SixtApi
) : CarRemoteDataSource {

    override fun getCars(): Single<List<Car>> = api.getAllCars()

}