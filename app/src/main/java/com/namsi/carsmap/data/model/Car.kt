package com.namsi.carsmap.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "car")
data class Car (
    @PrimaryKey
    var id : String,
    var modelIdentifier : String,
    var modelName : String,
    var name : String,
    var make : String,
    var group : String,
    var color : String,
    var series : String,
    var fuelType : String,
    var fuelLevel : Double,
    var transmission : String,
    var licensePlate : String,
    var latitude : Double,
    var longitude : Double,
    var innerCleanliness : String,
    var carImageUrl : String
)