package com.namsi.carsmap.data.source.remote.abstraction

import com.namsi.carsmap.data.model.Car
import io.reactivex.Single

interface CarRemoteDataSource {
    fun getCars(): Single<List<Car>>
}