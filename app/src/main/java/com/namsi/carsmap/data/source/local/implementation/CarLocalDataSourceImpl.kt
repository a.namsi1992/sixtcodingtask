package com.namsi.carsmap.data.source.local.implementation

import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.data.source.local.abstraction.CarLocalDataSource
import com.namsi.carsmap.data.source.local.abstraction.room.dao.CarDao
import io.reactivex.Single

class CarLocalDataSourceImpl(
    private val carDao: CarDao
) : CarLocalDataSource {

    override fun getCars(): Single<List<Car>> = carDao.getAllCars()

    override fun addCars(cars: List<Car>) {
        carDao.insertAllCars(cars)
    }

    override fun getCarById(carId: String): Single<Car> = carDao.getCarById(carId)

}