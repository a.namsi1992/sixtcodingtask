package com.namsi.carsmap.data.source.local.abstraction

import com.namsi.carsmap.data.model.Car
import io.reactivex.Single

interface CarLocalDataSource {
    fun getCars(): Single<List<Car>>
    fun addCars(cars: List<Car>)
    fun getCarById(carId: String): Single<Car>
}