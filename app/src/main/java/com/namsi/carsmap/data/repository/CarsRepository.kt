package com.namsi.carsmap.data.repository

import com.namsi.carsmap.data.model.Car
import io.reactivex.Single

interface CarsRepository {

    fun getCars(): Single<List<Car>>
    fun getCarById(carId: String): Single<Car>

}