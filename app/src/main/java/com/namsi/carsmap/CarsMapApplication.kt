package com.namsi.carsmap

import android.app.Application
import com.namsi.carsmap.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CarsMapApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialization of Dependency Injection library
        startKoin {
            androidContext(this@CarsMapApplication)

            modules(
                listOf(
                    viewModelModule,
                    repositoryModule,
                    dataSourceModule,
                    networkModule,
                    dbModule
                )
            )
        }
    }
}