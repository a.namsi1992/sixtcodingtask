package com.namsi.carsmap.utils

import androidx.lifecycle.MutableLiveData
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.presentation.model.ResourceStatus
import org.json.JSONObject
import retrofit2.HttpException


/**
 * This method check if device
 *  is connected to the Internet
 * @return [Boolean]
 */
//fun Context.isDeviceConnectedToInternet(): Boolean {
//    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//    val activeNetwork = cm.activeNetworkInfo
//    return (activeNetwork != null)
//}

fun MutableLiveData<Resource>.setLoading() {

    postValue(Resource(status = ResourceStatus.LOADING, data = null, message = null))
}

fun MutableLiveData<Resource>.setSuccess(data: Any?) {

    postValue(Resource(status = ResourceStatus.SUCCESS, data = data, message = null))
}

fun MutableLiveData<Resource>.setError(error: Throwable) {

    if (error is HttpException) postValue(
        Resource(
            status = ResourceStatus.ERROR,
            data = error.code(),
            message = error.fetchError()
        )
    )
    else postValue(Resource(status = ResourceStatus.ERROR, data = null, message = null))
}

/**
 * fetch error from httpException
 * @param
 * @return
 */
fun HttpException.fetchError(): String? {
    val errorMessage: String?
    val body = this.response()?.errorBody()!!.string()
    errorMessage = try {
        val jsonObject = JSONObject(body)
        jsonObject.getString("message")
    } catch (e: Exception) {
        e.message
    }

    return errorMessage
}