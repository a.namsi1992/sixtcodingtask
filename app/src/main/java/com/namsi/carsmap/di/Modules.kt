package com.namsi.carsmap.di

import androidx.room.Room
import com.namsi.carsmap.data.repository.CarsRepositoryImpl
import com.namsi.carsmap.data.source.local.abstraction.CarLocalDataSource
import com.namsi.carsmap.data.source.local.abstraction.room.SixtDataBase
import com.namsi.carsmap.data.source.local.implementation.CarLocalDataSourceImpl
import com.namsi.carsmap.data.source.remote.abstraction.CarRemoteDataSource
import com.namsi.carsmap.data.source.remote.implementation.CarRemoteDataSourceImpl
import com.namsi.carsmap.data.source.remote.implementation.sixtApi
import com.namsi.carsmap.data.repository.CarsRepository
import com.namsi.carsmap.presentation.cardetail.CarDetailViewModel
import com.namsi.carsmap.presentation.carmap.CarsViewModel
import com.namsi.carsmap.utils.DB_NAME
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModule: Module = module {
    viewModel { CarsViewModel(get()) }
    viewModel { CarDetailViewModel(get()) }
}

val networkModule: Module = module {
    single { sixtApi }
}

val dataSourceModule: Module = module {
    single { CarLocalDataSourceImpl(get()) as CarLocalDataSource }
    single { CarRemoteDataSourceImpl(sixtApi) as CarRemoteDataSource }
}

val repositoryModule: Module = module {
    single { CarsRepositoryImpl(get(), get()) as CarsRepository }
}

val dbModule: Module = module {
    single { Room.databaseBuilder(androidApplication(), SixtDataBase::class.java, DB_NAME).build() }
    single { get<SixtDataBase>().carDao() }
}
