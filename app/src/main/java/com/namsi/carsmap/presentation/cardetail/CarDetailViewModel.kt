package com.namsi.carsmap.presentation.cardetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.namsi.carsmap.data.repository.CarsRepository
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.utils.setError
import com.namsi.carsmap.utils.setLoading
import com.namsi.carsmap.utils.setSuccess
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CarDetailViewModel(
    private val carsRepository: CarsRepository
) : ViewModel() {

    val carLiveData = MutableLiveData<Resource>()

    private val compositeDisposable = CompositeDisposable()

    fun getCarById(carId: String) {
        compositeDisposable.add(carsRepository.getCarById(carId)
            .doOnSubscribe { carLiveData.setLoading() }
            .subscribeOn(Schedulers.io())
            .subscribe({
                carLiveData.setSuccess(it)
            }, {
                carLiveData.setError(it)
            }
            ))
    }

}