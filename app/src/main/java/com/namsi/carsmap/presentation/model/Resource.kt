package com.namsi.carsmap.presentation.model

data class Resource(
    val status: ResourceStatus,
    val data: Any?,
    val message: String?
)