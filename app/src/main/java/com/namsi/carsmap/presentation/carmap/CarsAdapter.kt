package com.namsi.carsmap.presentation.carmap

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.namsi.carsmap.R
import com.namsi.carsmap.data.model.Car
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_car.view.*

class CarAdapter(
    val items: MutableList<Car>,
    private val showInMapListener: (Int) -> Unit,
    private val showDetailsListener: (Int) -> Unit
) :
    RecyclerView.Adapter<CarViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        return CarViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_car, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(items[position], showInMapListener, showDetailsListener)
    }

}

class CarViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    @SuppressLint("SetTextI18n")
    fun bind(item: Car, showInMapListener: (Int) -> Unit, showDetailsListener: (Int) -> Unit) {
        Picasso.get().load(item.carImageUrl)
            .placeholder(R.drawable.car_placeholder)
            .error(R.drawable.car_placeholder)
            .into(view.car_image)
        view.car_model.text = item.modelName
        view.car_license_plate.text = item.licensePlate
        view.fuel_type.text = item.fuelType
        view.fuel_level.text = "${(item.fuelLevel * 100).toInt()}%"
        view.transmission_type.text = item.transmission
        view.show_car_in_map.setOnClickListener { showInMapListener(adapterPosition) }
        view.show_car_details.setOnClickListener { showDetailsListener(adapterPosition) }
    }
}