package com.namsi.carsmap.presentation.model

enum class ResourceStatus(val value: Int) {
    LOADING(0),
    SUCCESS(1),
    ERROR(-1)
}