package com.namsi.carsmap.presentation.cardetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.namsi.carsmap.R
import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.presentation.model.ResourceStatus
import com.namsi.carsmap.utils.ConnectivityLiveData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_car_detail.*
import org.koin.android.viewmodel.ext.android.viewModel

class CarDetailActivity : AppCompatActivity() {

    private val carViewModel: CarDetailViewModel by viewModel()

    private lateinit var car: Car
    private lateinit var offlineSnackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_detail)

        // toolbar configuration
        setSupportActionBar(detailToolbar)
        detailToolbar.setNavigationOnClickListener { finish() }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        // the offline snackbar declaration
        offlineSnackbar = Snackbar.make(
            item_detail_container,
            resources.getString(R.string.no_connection),
            Snackbar.LENGTH_INDEFINITE
        )

        //observer on device connectivity
        ConnectivityLiveData(this)
            .observe(
                this,
                Observer<Boolean> {
                    when (it) {
                        false -> offlineSnackbar.show()
                        true -> offlineSnackbar.dismiss()
                    }
                })

        carViewModel.carLiveData.observe(this, carObsserver)

        intent.getStringExtra("carId")?.let { carViewModel.getCarById(it) }
    }

    // populate view with data
    @SuppressLint("SetTextI18n")
    private fun updateView() {
        Picasso.get().load(car.carImageUrl)
            .placeholder(R.drawable.car_placeholder)
            .error(R.drawable.car_placeholder)
            .into(toolbarImage)
        detailToolbar.title = car.name
        car_model.text = "${car.make} - ${car.modelName}"
        fuel_type.text = car.fuelType
        fuel_level.text = "${(car.fuelLevel * 100).toInt()}"
        transmission_type.text = car.transmission
        car_license_plate.text = car.licensePlate
        color_text.text = car.color
        cleanliness_text.text = car.innerCleanliness
    }

    private val carObsserver: Observer<in Resource> = Observer {
        when (it.status) {
            ResourceStatus.SUCCESS -> {
                car = (it.data as Car)
                updateView()
            }
        }
    }

}