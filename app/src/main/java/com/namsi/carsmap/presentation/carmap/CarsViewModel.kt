package com.namsi.carsmap.presentation.carmap

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.namsi.carsmap.data.repository.CarsRepository
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.utils.ConnectivityLiveData
import com.namsi.carsmap.utils.setError
import com.namsi.carsmap.utils.setLoading
import com.namsi.carsmap.utils.setSuccess
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CarsViewModel(
    private val carsRepository: CarsRepository
) : ViewModel() {

    val carsLiveData = MutableLiveData<Resource>()

    private val compositeDisposable = CompositeDisposable()

    fun getCars() {
        compositeDisposable.add(carsRepository.getCars()
            .doOnSubscribe { carsLiveData.setLoading() }
            .subscribeOn(Schedulers.io())
            .subscribe({
                carsLiveData.setSuccess(it)
            }, {
                carsLiveData.setError(it)
            }
            ))
    }

}