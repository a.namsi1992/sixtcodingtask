package com.namsi.carsmap.presentation.carmap

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.namsi.carsmap.R
import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.presentation.cardetail.CarDetailActivity
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.presentation.model.ResourceStatus
import com.namsi.carsmap.utils.ConnectivityLiveData
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel


class CarsMapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var offlineSnackbar: Snackbar
    private val carsViewModel: CarsViewModel by viewModel()

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var mMap: GoogleMap

    private val carsList = ArrayList<Car>()
    private val carsAdapter: CarAdapter =
        CarAdapter(carsList, { position: Int ->
            zoomToMarker(carsList[position])
        }, { position: Int ->
            val intent = Intent(this, CarDetailActivity::class.java)
            intent.putExtra("carId", carsList[position].id)
            startActivity(intent)
        })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        offlineSnackbar = Snackbar.make(
            car_map_main_layout, resources.getString(R.string.no_connection),
            Snackbar.LENGTH_INDEFINITE
        )
        val view = offlineSnackbar.view
        val params = view.layoutParams as CoordinatorLayout.LayoutParams
        params.gravity = Gravity.TOP
        view.layoutParams = params

        carsViewModel.carsLiveData.observe(this, carsObserver)

        cars_recycler.layoutManager = LinearLayoutManager(this as AppCompatActivity)
        cars_recycler.adapter = carsAdapter

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_navigation_container)
        bottomSheetBehavior.addBottomSheetCallback(carsListSheetCallback)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        theme_switch.setOnCheckedChangeListener { _, b ->
            if (b) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

        expand_arrow.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            else bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        ConnectivityLiveData(this)
            .observe(this, {
                when (it) {
                    false -> offlineSnackbar.show()
                    true -> {
                        offlineSnackbar.dismiss()
                        if (carsList.isEmpty())
                            carsViewModel.getCars()
                    }
                }
            })
    }

    private fun zoomToMarker(car: Car) {
        val googlePlex = CameraPosition.builder()
            .target(LatLng(car.latitude, car.longitude))
            .zoom(15f)
            .bearing(0f)
            .build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex))
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            return
        }
        super.onBackPressed()
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.night_map))
        carsViewModel.getCars()
    }

    private val carsListSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                expand_arrow.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_expand_more_24))
            }

            if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                expand_arrow.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_expand_less_24))
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            if (slideOffset > 0.5)
                expand_arrow.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_expand_more_24))
            else expand_arrow.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_expand_less_24))

        }
    }

    private val carsObserver: Observer<in Resource> = Observer {
        when (it.status) {
            ResourceStatus.SUCCESS -> {
                if (this::mMap.isInitialized) {
                    carsList.clear()
                    carsList.addAll(it.data as List<Car>)
                    carsAdapter.notifyDataSetChanged()
                    carsList.forEach {
                        val m = MarkerOptions()
                            .title(it.name)
                            .position(LatLng(it.latitude, it.longitude))
                        m.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_marker))
                        mMap.addMarker(m)
                    }
                    if (carsList.isNotEmpty()) {
                        zoomToMarker(carsList[0])
                        empty_list_image.visibility = View.GONE
                        cars_recycler.visibility = View.VISIBLE
                    } else {
                        empty_list_image.visibility = View.VISIBLE
                        cars_recycler.visibility = View.GONE

                    }
                }
            }
        }
    }

}