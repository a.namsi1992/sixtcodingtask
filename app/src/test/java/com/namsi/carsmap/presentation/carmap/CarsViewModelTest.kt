package com.namsi.carsmap.presentation.carmap

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.namsi.carsmap.car1
import com.namsi.carsmap.car2
import com.namsi.carsmap.data.repository.CarsRepository
import com.namsi.carsmap.presentation.RxSchedulersOverrideRule
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.presentation.model.ResourceStatus
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CarsViewModelTest {

    private lateinit var carsViewModel: CarsViewModel

    @Mock
    private lateinit var carsRepository: CarsRepository

    private val cars = listOf(car1, car2)
    private val throwable = Throwable()


    @Rule
    @JvmField
    val rxSchedulersOverrideRule = RxSchedulersOverrideRule()

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        carsViewModel = CarsViewModel(carsRepository)
    }

    @Test
    fun test_getCars_Success() {

        Mockito.`when`(carsRepository.getCars()).thenReturn(Single.just(cars))

        carsViewModel.getCars()

        Mockito.verify(carsRepository).getCars()
        assertEquals(
            Resource(
                ResourceStatus.SUCCESS,
                cars,
                null
            ),
            carsViewModel.carsLiveData.value
        )
    }

    @Test
    fun test_getCars_Failure() {

        Mockito.`when`(carsRepository.getCars()).thenReturn(Single.error(throwable))

        carsViewModel.getCars()

        Mockito.verify(carsRepository).getCars()
        assertEquals(
            Resource(
                ResourceStatus.ERROR,
                null,
                throwable.message
            ),
            carsViewModel.carsLiveData.value
        )
    }
}