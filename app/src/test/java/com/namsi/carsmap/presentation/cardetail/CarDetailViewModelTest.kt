package com.namsi.carsmap.presentation.cardetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.namsi.carsmap.car1
import com.namsi.carsmap.car2
import com.namsi.carsmap.data.repository.CarsRepository
import com.namsi.carsmap.presentation.RxSchedulersOverrideRule
import com.namsi.carsmap.presentation.model.Resource
import com.namsi.carsmap.presentation.model.ResourceStatus
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CarDetailViewModelTest {

    private lateinit var detailViewModel: CarDetailViewModel

    @Mock
    private lateinit var carsRepository: CarsRepository

    private val cars = listOf(car1, car2)
    private val throwable = Throwable()


    @Rule
    @JvmField
    val rxSchedulersOverrideRule = RxSchedulersOverrideRule()

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        detailViewModel = CarDetailViewModel(carsRepository)
    }

    @Test
    fun test_getCar_Success() {

        val carId = "WMWSW310X0T114073"

        Mockito.`when`(carsRepository.getCarById(carId)).thenReturn(Single.just(cars.filter { it.id == carId }[0]))

        detailViewModel.getCarById(carId)

        Mockito.verify(carsRepository).getCarById(carId)
        assertEquals(
            Resource(
                ResourceStatus.SUCCESS,
                cars.filter { it.id == carId }[0],
                null
            ),
            detailViewModel.carLiveData.value
        )
    }

    @Test
    fun test_getCar_Failure() {

        val carId = "WMWSW310X0T114073"

        Mockito.`when`(carsRepository.getCarById(carId)).thenReturn(Single.error(throwable))

        detailViewModel.getCarById(carId)

        Mockito.verify(carsRepository).getCarById(carId)
        assertEquals(
            Resource(
                ResourceStatus.ERROR,
                null,
                throwable.message
            ),
            detailViewModel.carLiveData.value
        )
    }
}