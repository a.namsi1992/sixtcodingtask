package com.namsi.carsmap.data.source.local

import com.namsi.carsmap.car1
import com.namsi.carsmap.car2
import com.namsi.carsmap.data.source.local.abstraction.room.dao.CarDao
import com.namsi.carsmap.data.source.local.implementation.CarLocalDataSourceImpl
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CarsLocalDataSourceImplTest {

    private lateinit var dataSource: CarLocalDataSourceImpl

    @Mock
    private lateinit var mockDoa: CarDao

    private val throwable = Throwable()

    private val cars = listOf(car1, car2)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataSource = CarLocalDataSourceImpl(mockDoa)
    }


    @Test
    fun test_getCars_Success() {

        Mockito.`when`(mockDoa.getAllCars()).thenReturn(Single.just(cars))

        val test = dataSource.getCars().test()

        Mockito.verify(mockDoa).getAllCars()
        test.assertValue(cars)
    }

    @Test
    fun test_getCars_Failure() {

        Mockito.`when`(mockDoa.getAllCars()).thenReturn(Single.error(throwable))

        val test = dataSource.getCars().test()


        Mockito.verify(mockDoa).getAllCars()
        test.assertError(throwable)
    }

    @Test
    fun test_getCarById_Success() {

        val carId = "WMWSW310X0T114073"

        Mockito.`when`(mockDoa.getCarById(carId))
            .thenReturn(Single.just(cars.filter { it.id == carId }[0]))


        val test = dataSource.getCarById(carId).test()

        Mockito.verify(mockDoa).getCarById(carId)
        test.assertValue(cars.filter { it.id == carId }[0])
    }

    @Test
    fun test_getCarById_Failure() {
        val carId = "WMWSW310X0T114073"

        Mockito.`when`(mockDoa.getCarById(carId)).thenReturn(Single.error(throwable))

        val test = dataSource.getCarById(carId).test()


        Mockito.verify(mockDoa).getCarById(carId)

        test.assertError(throwable)
    }
}