package com.namsi.carsmap.data.repository

import com.namsi.carsmap.car1
import com.namsi.carsmap.car2
import com.namsi.carsmap.data.model.Car
import com.namsi.carsmap.data.source.local.abstraction.CarLocalDataSource
import com.namsi.carsmap.data.source.remote.abstraction.CarRemoteDataSource
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CarsRepositoryImplTest {

    private lateinit var repository: CarsRepositoryImpl

    @Mock
    private lateinit var mockRemoteDataSource: CarRemoteDataSource

    @Mock
    private lateinit var mockLocalDataSource: CarLocalDataSource

    private val throwable = Throwable()

    private val cars = listOf(car1, car2)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = CarsRepositoryImpl(mockLocalDataSource, mockRemoteDataSource)
    }


    @Test
    fun test_getCars_RemoteDataSource_Success() {

        Mockito.`when`(mockRemoteDataSource.getCars()).thenReturn(Single.just(cars))


        val test = repository.getCars().test()

        Mockito.verify(mockRemoteDataSource).getCars()
        test.assertValue(cars)
    }

    @Test
    fun test_getCars_RemoteDataSource_Failure_LocalDataSource_Success() {

        Mockito.`when`(mockRemoteDataSource.getCars()).thenReturn(Single.error(throwable))
        Mockito.`when`(mockLocalDataSource.getCars()).thenReturn(Single.just(cars))

        val test = repository.getCars().test()


        Mockito.verify(mockRemoteDataSource).getCars()
        Mockito.verify(mockLocalDataSource).getCars()

        test.assertValue(cars)
    }

    @Test
    fun test_getCars_RemoteDataSource_Failure_LocalDataSource_Failure() {

        Mockito.`when`(mockRemoteDataSource.getCars()).thenReturn(Single.error(throwable))
        Mockito.`when`(mockLocalDataSource.getCars()).thenReturn(Single.error(throwable))

        val test = repository.getCars().test()


        Mockito.verify(mockRemoteDataSource).getCars()
        Mockito.verify(mockLocalDataSource).getCars()

        test.assertError(throwable)
    }

    @Test
    fun test_getCarById_Success() {

        val carId = "WMWSW310X0T114073"

        Mockito.`when`(mockLocalDataSource.getCarById(carId)).thenReturn(Single.just(cars.filter { it.id == carId }[0]))


        val test = repository.getCarById(carId).test()

        Mockito.verify(mockLocalDataSource).getCarById(carId)
        test.assertValue(cars.filter { it.id == carId }[0])
    }

    @Test
    fun test_getCarById_Failure() {

        val carId = "WMWSW310X0T114073"

        Mockito.`when`(mockLocalDataSource.getCarById(carId)).thenReturn(Single.error(throwable))

        val test = repository.getCarById(carId).test()


        Mockito.verify(mockLocalDataSource).getCarById(carId)

        test.assertError(throwable)
    }
}