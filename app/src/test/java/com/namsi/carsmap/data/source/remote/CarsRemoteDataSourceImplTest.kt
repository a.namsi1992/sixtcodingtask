package com.namsi.carsmap.data.source.remote

import com.namsi.carsmap.car1
import com.namsi.carsmap.car2
import com.namsi.carsmap.data.source.local.abstraction.room.dao.CarDao
import com.namsi.carsmap.data.source.local.implementation.CarLocalDataSourceImpl
import com.namsi.carsmap.data.source.remote.abstraction.SixtApi
import com.namsi.carsmap.data.source.remote.implementation.CarRemoteDataSourceImpl
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CarsRemoteDataSourceImplTest {

    private lateinit var dataSource: CarRemoteDataSourceImpl

    @Mock
    private lateinit var mockApi: SixtApi

    private val throwable = Throwable()

    private val cars = listOf(car1, car2)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataSource = CarRemoteDataSourceImpl(mockApi)
    }


    @Test
    fun test_getCars_Success() {

        Mockito.`when`(mockApi.getAllCars()).thenReturn(Single.just(cars))

        val test = dataSource.getCars().test()

        Mockito.verify(mockApi).getAllCars()
        test.assertValue(cars)
    }

    @Test
    fun test_getCars_Failure() {

        Mockito.`when`(mockApi.getAllCars()).thenReturn(Single.error(throwable))

        val test = dataSource.getCars().test()


        Mockito.verify(mockApi).getAllCars()
        test.assertError(throwable)
    }

}