package com.namsi.carsmap

import com.namsi.carsmap.data.model.Car
import org.junit.Test

import org.junit.Assert.*

val car1 = Car(id="WMWSW310X0T114073", modelIdentifier="mini", modelName="MINI", name="Siegfried", make="BMW", group="MINI", color="midnight_black", series="MINI", fuelType="D", fuelLevel=0.85, transmission="M", licensePlate="M-IL2647", latitude=48.137342, longitude=11.546826, innerCleanliness="REGULAR", carImageUrl="https://cdn.sixt.io/codingtask/images/mini.png")

val car2 = Car(id="WMWXM51090T900602", modelIdentifier="mini", modelName="MINI", name="Silvia", make="BMW", group="MINI", color="midnight_black_metal", series="MINI", fuelType="P", fuelLevel=0.72, transmission="M", licensePlate="M-DX8561", latitude=48.189907, longitude=11.574388, innerCleanliness="CLEAN", carImageUrl="https://cdn.sixt.io/codingtask/images/mini.png")
